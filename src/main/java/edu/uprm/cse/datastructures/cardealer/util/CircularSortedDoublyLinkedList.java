package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	int currentSize;
	private Node<E> header;
	private Comparator<E> comp;

	//Constructor
	public CircularSortedDoublyLinkedList() {;
	header = new Node<E>();
	header.setNext(this.header);
	header.setPrev(this.header);
	currentSize = 0;
	}

	public CircularSortedDoublyLinkedList(Comparator<E> comp) {;
	header = new Node<E>();
	header.setNext(this.header);
	header.setPrev(this.header);
	currentSize = 0;
	this.comp = comp;
	}
	
	//Iterator class has yet to be used
	private class PositionalListIterator<E> implements Iterator<E>{
		private Node<E> currentPosition;

		private PositionalListIterator() {
			this.currentPosition = (Node<E>) header.getNext();
		}
		public boolean hasNext() {
			return this.currentPosition != header;
		}


		public E next() {
			if (hasNext()) {
				E result = this.currentPosition.getElement();
				this.currentPosition = this.currentPosition.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}
	
	//creates an instance of an iterator, but has yet to be used
	public Iterator iterator() {
		return new PositionalListIterator<Car>();
	}
	
	//adds a car to the list
	@Override
	public boolean add(E obj) {
		Node<E> newNode = new Node<E>(obj);
		if (isEmpty()) {

			header.setNext(newNode);
			header.setPrev(newNode);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			currentSize++;
			return true;
		}
		Node<E> curr = header.getNext();
		while (curr != header) {
			if (comp.compare(obj, curr.getElement()) <= 0) {
				newNode.setNext(curr);
				newNode.setPrev(curr.getPrev());
				curr.getPrev().setNext(newNode);
				curr.setPrev(newNode);
				currentSize++;
				return true;
			}
			curr = curr.getNext();
		}
		header.getPrev().setNext(newNode);
		newNode.setPrev(header.getPrev());
		newNode.setNext(header);
		header.setPrev(newNode);
		currentSize++;
		return true;
	}
	
	//returns the current size of the list
	@Override
	public int size() {
		return currentSize;
	}
	
	//removes a given object obj from the list, if it is on the list
	@Override
	public boolean remove(E obj) {
		Node<E> curr = header;
		while (curr.getNext() != header) {
			if (comp.compare(obj, curr.getNext().getElement()) == 0) {
				Node<E> ntr = curr.getNext();
				Node<E> ref = curr.getNext().getNext();
				curr.setNext(ref);
				ref.setPrev(curr);
				ntr.setElement(null);
				ntr.setNext(null);
				ntr.setPrev(null);
				currentSize--;
				return true;
			}
			curr = curr.getNext();
		}
		return false;
	}
	
	//removes an object from the list, if the given index exists
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException{
		if(index < 0 || index > currentSize - 1) {
			throw new IndexOutOfBoundsException("Error: index to remove is bigger than list size");
		}else {
			Node<E> curr = header.getNext();
			for (int i = 0; i < index; i++) {
				curr = curr.getNext();
			}
			curr.getPrev().setNext(curr.getNext());
			curr.getNext().setPrev(curr.getPrev());
			curr.setElement(null);
			curr.setNext(null);
			curr.setPrev(null);
			currentSize--;
			return true;
		}
	}
	
	//removes all occurrences of a given object from the list
	@Override
	public int removeAll(E obj) {
		Node<E> curr = header;
		int instances = 0;
		while (curr.getNext() != header) {
			if (curr.getNext().getElement().equals(obj))  {
				Node<E> ntr = curr.getNext();
				ntr.getPrev().setNext(ntr.getNext());
				ntr.getNext().setPrev(ntr.getPrev());
				ntr.setElement(null);
				ntr.setNext(null);
				ntr.setPrev(null);
				curr = curr.getPrev();
				currentSize--;
				instances++;
			}
			curr = curr.getNext();
		}
		return instances;
	}
	
	//returns the last object in the list
	@Override
	public E last() {
		return this.isEmpty() ? null: this.header.getPrev().getElement();
	}
	
	//returns the first object in the list
	@Override
	public E first() {
		return this.isEmpty() ? null: this.header.getNext().getElement();
	}
	
	//returns the object in the list 
	@Override
	public E get(int index) {
		if(index > this.currentSize || index < 0) {
			throw new IndexOutOfBoundsException("Index exceeds size of cars list.");
		}
		Node<E> curr = header.getNext();
		for(int i = 0; i < index; i++) {
			curr = curr.getNext();
		}
		return curr.getElement();

	}
	
	//clears the list
	@Override
	public void clear() {
		while(!this.isEmpty())
			remove(0);
	}
	
	//returns true if the object e is found within the list, false otherwise
	@Override
	public boolean contains(E e) {
		Node<E> curr = header.getNext();
		while (curr != header) {
			if (curr.getElement().equals(e)) {
				return true;
			}
			curr = curr.getNext();
		}
		return false;
	}
	
	//checks if the list is empty (true if it is, false otherwise)
	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}
	
	//returns the first time an object e appears on the list
	@Override
	public int firstIndex(E e) {
		if(this.contains(e)) {
			Node<E> curr = header.getNext();
			for(int i = 0; i < this.currentSize; i++) {
				if (curr.getElement().equals(e)) {
					return i;
				}
				curr = curr.getNext();
			}
		}
		return -1;
	}
	
	//returns the last time an object e appears on the list
	@Override
	public int lastIndex(E e) {
		if(this.contains(e)) {
			Node<E> curr = header.getPrev();
			for(int i = this.currentSize-1; i > 0; i--) {
				if (curr.getElement().equals(e)) {
					return i;
				}
				curr = curr.getPrev();
			}
		}
		return -1;
	}
}
