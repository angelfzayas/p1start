package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {



	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	//method to get all the cars
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCars() {
		Car[] result = new Car[cList.size()];
		for (int i = 0; i < cList.size(); i++) {
			result[i] = cList.get(i);
		}return result;
	}

	//method to return the cars which IDs matches the given one
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == id) {
				return cList.get(i);
			}
		}throw new WebApplicationException(404);
	}

	//method to add cars to the array list
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		cList.add(car);
		return Response.status(Response.Status.CREATED).build();
	}

	//method that removes a specific car while adding a new one 
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == car.getCarId()) {
				cList.remove(i);
				cList.add(car);
				return Response.status(200).build();
			}
		}return Response.status(Response.Status.NOT_FOUND).build();
	}

	//method that gets rid of a car from the array
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(200).build();
			}
		}return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}