package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car obj1, Car obj2) {
		//info from the first car
		String car1 = (obj1.getCarBrand() + obj1.getCarModel() + obj1.getCarModelOption());
		// info from the second car
		String car2 = (obj2.getCarBrand() + obj2.getCarModel() + obj2.getCarModelOption());
		// comparing them
		return car1.compareTo(car2);
	}


}
