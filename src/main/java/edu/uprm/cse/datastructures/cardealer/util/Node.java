package edu.uprm.cse.datastructures.cardealer.util;

public class Node<E>{
	public E element;
	public Node<E> prev;
	public Node<E> next;
	
	public Node(E element, Node<E> prev, Node<E> next){
		this.element = element;
		this.next = next;
		this.prev = prev;
	}
	
	public Node() {
		this.element = null;
		this.next = null;
		this.prev = null;
	}
	
	public void setElement(E e) {
		this.element = e;
	}
	
	public Node(E element) {
		this.element = element;
	}

	public E getElement(){
		return element;
	}

	public void setPrev(Node<E> prev){
		this.prev = prev;
	}

	public void setNext(Node<E> next){
		this.next = next;
	}

	public Node<E> getPrev(){
		return this.prev;
	}

	public Node<E> getNext(){
		return this.next;
	}

}
